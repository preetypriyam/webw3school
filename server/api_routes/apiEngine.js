/**
 * Created by NB on 26-Jan-16.
 */
let apiList = require("./apiList");
module.exports = function (app) {
    for (let methd1 in apiList) {
        let methd = methd1;
        for (let pth in apiList[methd]) {
            let i = pth;
            app[methd](apiList[methd][i].url, apiList[methd][i].callback)
        }
    }
};