var path = require("path"),
    constantApi = require("../controllers/constantApi");
module.exports = {
    "get": [
        {
            "url": ["/"],
            "callback": function (req, res, next) {
                res.sendFile('index.html', {root: path.join(__dirname, '../../client/dist')});
            }
        },
        {
            "url": "/get_constants",
            "callback": function (req, res, next) {
                constantApi.getConstants(req.query, res);
            }
        }
    ],
    "post": [],
    "put": []
};


