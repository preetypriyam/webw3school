var express = require('express'),
	app = express();
var conf= require("./conf/config");

var middleParse = require('./common/middleware.js');
middleParse.middleware(app,express);


var addApiList = require("./api_routes/apiEngine");
addApiList(app);

var srvr=app.listen(conf.server.port, function() {
	console.log('listening on port : ' + conf.server.port);
});