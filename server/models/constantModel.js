const mong = require('mongodb');
const MongoClient = mong.MongoClient;
const conf = require('../conf/config');

module.exports = {
    getConstants: function (query, res) {
        MongoClient.connect(conf.Local_MONGODB_URI, function (err, db) {
            if (err) {
                console.log("err: ", err)
            }
            let collection = db.collection('constants');
            collection.find(query).toArray(function (err, docs) {

                res.send(docs);
            });
        });
    },
};