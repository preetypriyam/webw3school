import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {RootNav, IIconItem} from '../../types';
import {iconItems, topNavItem} from '../../utils';

@Component({
  selector: 'app-root-content',
  templateUrl: './root-content.component.html',
  styleUrls: ['./root-content.component.css']
})
export class RootContentComponent implements OnInit {
  localTopNavItems: RootNav[];
  localIconItems: IIconItem[];

  constructor(private router: Router) {
    this.localTopNavItems = topNavItem;
    this.localIconItems = iconItems;
  }

  ngOnInit(): void {
  }

}

