import {Component, Input, OnInit} from '@angular/core';
import {IIconItem, TopNavItem} from '../../../types';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  @Input() myTopNavItems: TopNavItem[];
  @Input() rightTopNav: IIconItem[];
  currentOpenTab: string;

  constructor() {
  }

  ngOnInit(): void {
  }

  onNavClick(navValue: string): void {
    console.log({navValue});
    if (navValue === this.currentOpenTab) {
      this.currentOpenTab = '';
    } else {
      this.currentOpenTab = navValue;
    }
  }
}
