import {Component, OnInit} from '@angular/core';
import {INavItem} from '../../../types';
import {htmlAndCss} from '../../../utils';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {
  cssNavItems: INavItem [];

  constructor() {
    this.cssNavItems = htmlAndCss;
  }

  ngOnInit(): void {
  }

}
