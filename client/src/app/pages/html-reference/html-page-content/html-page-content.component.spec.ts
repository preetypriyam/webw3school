import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HtmlPageContentComponent} from './html-page-content.component';

describe('HtmlPageContentComponent', () => {
  let component: HtmlPageContentComponent;
  let fixture: ComponentFixture<HtmlPageContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HtmlPageContentComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HtmlPageContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
