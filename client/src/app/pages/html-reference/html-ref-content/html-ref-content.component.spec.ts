import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HtmlRefContentComponent} from './html-ref-content.component';

describe('HtmlRefContentComponent', () => {
  let component: HtmlRefContentComponent;
  let fixture: ComponentFixture<HtmlRefContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HtmlRefContentComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HtmlRefContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
