import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HtmlRefSidebarComponent} from './html-ref-sidebar.component';

describe('HtmlRefSidebarComponent', () => {
  let component: HtmlRefSidebarComponent;
  let fixture: ComponentFixture<HtmlRefSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HtmlRefSidebarComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HtmlRefSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
