import {Component, Input, OnInit} from '@angular/core';
import {RefNevItem} from '../../../types';

@Component({
  selector: 'app-html-ref-sidebar',
  templateUrl: './html-ref-sidebar.component.html',
  styleUrls: ['./html-ref-sidebar.component.css']
})
export class HtmlRefSidebarComponent implements OnInit {
  @Input() mySideNav: RefNevItem[];
  @Input() title: string;

  constructor() {
  }

  ngOnInit(): void {
  }

}
