import {Component, OnInit} from '@angular/core';
import {RefNevItem, IIconItem, TopNavItem} from '../../types';
import {topNav, topNavDropdown, iconItems, refNavs} from '../../utils';

@Component({
  selector: 'app-html-reference',
  templateUrl: './html-reference.component.html',
  styleUrls: ['./html-reference.component.css']
})
export class HtmlReferenceComponent implements OnInit {
  localSideRef: RefNevItem[];
  localTopNav: TopNavItem [];
  topDropdown: TopNavItem[];
  localIconItems: IIconItem[];


  constructor() {
    this.localSideRef = refNavs;
    this.localTopNav = topNav;
    this.topDropdown = topNavDropdown;
    this.localIconItems = iconItems;
  }

  ngOnInit(): void {
  }

}
