import {Component, Input, OnInit} from '@angular/core';
import {IIconItem, TopNavItem} from '../../../types';

@Component({
  selector: 'app-html-nav',
  templateUrl: './html-nav.component.html',
  styleUrls: ['./html-nav.component.css']
})
export class HtmlNavComponent implements OnInit {
  @Input() myTopNavItems: TopNavItem[];
  @Input() topNavDropdown: TopNavItem[];
  @Input() rightTopNav: IIconItem[];

  constructor() {
  }

  ngOnInit(): void {
  }

}
