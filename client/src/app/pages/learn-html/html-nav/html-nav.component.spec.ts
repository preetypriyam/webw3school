import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HtmlNavComponent} from './html-nav.component';

describe('HtmlNavComponent', () => {
  let component: HtmlNavComponent;
  let fixture: ComponentFixture<HtmlNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HtmlNavComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HtmlNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
