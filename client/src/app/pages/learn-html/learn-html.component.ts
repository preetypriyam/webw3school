import {Component, OnInit} from '@angular/core';
import {topNav, topNavDropdown, htmlNavs, iconItems} from '../../utils';
import {IIconItem, INavItem, TopNavItem} from '../../types';

@Component({
  selector: 'app-learn-html',
  templateUrl: './learn-html.component.html',
  styleUrls: ['./learn-html.component.css']
})
export class LearnHtmlComponent implements OnInit {
  localNavItem: INavItem [];
  localTopNav: TopNavItem [];
  topDropdown: TopNavItem[];
  localIconItems: IIconItem[];

  constructor() {
    this.localNavItem = htmlNavs;
    this.localTopNav = topNav;
    this.topDropdown = topNavDropdown;
    this.localIconItems = iconItems;
  }

  ngOnInit(): void {
  }

}
