import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HtmlSideBarComponent} from './html-side-bar.component';

describe('HtmlSideBarComponent', () => {
  let component: HtmlSideBarComponent;
  let fixture: ComponentFixture<HtmlSideBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HtmlSideBarComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HtmlSideBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
