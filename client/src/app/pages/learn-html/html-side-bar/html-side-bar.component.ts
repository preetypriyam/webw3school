import {Component, Input, OnInit} from '@angular/core';
import {INavItem} from '../../../types';

@Component({
  selector: 'app-html-side-bar',
  templateUrl: './html-side-bar.component.html',
  styleUrls: ['./html-side-bar.component.css']
})
export class HtmlSideBarComponent implements OnInit {
  @Input() myNavItems: INavItem[];
  @Input() title: string;

  constructor() {
  }

  ngOnInit(): void {
  }

}
