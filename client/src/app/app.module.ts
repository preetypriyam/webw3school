import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';


import {AppRoutingModule} from './app-routing.module';
import {GetConstantsService} from './utils';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NavBarComponent} from './pages/home/nav-bar/nav-bar.component';
import {HeaderComponent} from './header/header.component';
import {SideBarComponent} from './pages/home/side-bar/side-bar.component';
import {PageContentComponent} from './pages/home/pageArea/top/page-content/page-content.component';
import {LearnHtmlComponent} from './pages/learn-html/learn-html.component';
import {HtmlReferenceComponent} from './pages/html-reference/html-reference.component';
import {RootContentComponent} from './pages/home/root-content.component';
import {HtmlNavComponent} from './pages/learn-html/html-nav/html-nav.component';
import {HtmlSideBarComponent} from './pages/learn-html/html-side-bar/html-side-bar.component';
import {HtmlPageContentComponent} from './pages/html-reference/html-page-content/html-page-content.component';
import {HtmlRefSidebarComponent} from './pages/html-reference/html-ref-sidebar/html-ref-sidebar.component';
import {HtmlRefContentComponent} from './pages/html-reference/html-ref-content/html-ref-content.component';
import {PageNavComponent} from './pages/home/pageArea/top/page-nav/page-nav.component';
import {BottomContentComponent} from './pages/home/pageArea/bottom/bottom-content/bottom-content.component';
import {BottomNavComponent} from './pages/home/pageArea/bottom/bottom-nav/bottom-nav.component';
import {
  TutorialsComponent,
  ReferencesComponent,
  ExamplesComponent,
  ExercisesComponent
} from './dropdown';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    HeaderComponent,
    SideBarComponent,
    PageContentComponent,
    LearnHtmlComponent,
    HtmlReferenceComponent,
    RootContentComponent,
    HtmlNavComponent,
    HtmlSideBarComponent,
    HtmlPageContentComponent,
    HtmlRefSidebarComponent,
    HtmlRefContentComponent,
    PageNavComponent,
    BottomContentComponent,
    BottomNavComponent,
    TutorialsComponent,
    ReferencesComponent,
    ExamplesComponent,
    ExercisesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
  ],
  providers: [GetConstantsService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
