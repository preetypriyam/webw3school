import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {ITutorialItem} from '../../types';
import {GetConstantsService} from '../../utils';

@Component({
  selector: 'app-tutorials',
  templateUrl: './tutorials.component.html',
  styleUrls: ['./tutorials.component.css']
})
export class TutorialsComponent implements OnInit, OnChanges {
  @Input() type: any;

  localTopicA: ITutorialItem[] = [];
  localTopicBFirst: ITutorialItem[] = [];
  localTopicBSecond: ITutorialItem[] = [];
  localTopicCFirst: ITutorialItem[] = [];
  localTopicCSecond: ITutorialItem[] = [];
  localTopicD: ITutorialItem[] = [];

  constructor(private constApi: GetConstantsService) {
    console.log('kugwqdiuqwg', this.type);
  }

  ngOnChanges(): void {
    console.log('qkwfiuqew', this.type);
    if (this.type) {
      console.log({type: this.type});
      const constantArr = ['aDropd', 'bFirstDropd', 'bSecondDropd', 'cFirstDropd', 'cSecondDropd', 'dDropd'];
      constantArr.forEach(comstantItem => {
        this.constApi.getConstants(comstantItem).subscribe(data => {
          const {type, value} = data[0];
          switch (type) {
            case constantArr[0]:
              this.localTopicA = value;
              break;
            case constantArr[1]:
              this.localTopicBFirst = value;
              break;
            case constantArr[2]:
              this.localTopicBSecond = value;
              break;
            case constantArr[3]:
              this.localTopicCFirst = value;
              break;
            case constantArr[4]:
              this.localTopicCSecond = value;
              break;
            case constantArr[5]:
              this.localTopicD = value;
              break;
            default:
              break;
          }
        });
      });
    }
  }

  ngOnInit(): void {
    console.log({type: this.type});
  }

}
