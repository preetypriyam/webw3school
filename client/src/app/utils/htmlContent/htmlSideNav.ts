const htmlNavs = [
  {text: 'HTML HOME', href: '/h1'},
  {text: 'HTML Introduction', href: '/h2'},
  {text: 'HTML Editors', href: '/h3'},
  {text: 'HTML Basic', href: '/h4'},
  {text: 'HTML Elements', href: '/h5'},
  {text: 'HTML Attributes', href: '/h6'},
  {text: 'HTML Headings', href: '/h7'},
  {text: 'HTML Paragraphs', href: '/h8'},
  {text: 'HTML Styles', href: '/h9'},
  {text: 'HTML Formatting', href: '/h10'},
  {text: 'HTML Quotations', href: '/h11'},
  {text: 'HTML Comments', href: '/h12'},
  {text: 'HTML Colors', href: '/h13'},
  {text: 'HTML CSS', href: '/h14'},
  {text: 'HTML Links', href: '/h15'},
  {text: 'HTML Images', href: '/h16'},
  {text: 'HTML Tables', href: '/h17'},
  {text: 'HTML Lists', href: '/h18'},
  {text: 'HTML Block & Inline', href: '/h19'},
  {text: 'HTML Classes', href: '/h20'},
  {text: 'HTML Id', href: '/h21'},
  {text: 'HTML Iframes', href: '/h22'},
  {text: 'HTML JavaScript', href: '/h23'},
  {text: 'HTML File Paths', href: '/h24'},
  {text: 'HTML Head', href: '/h25'},
  {text: 'HTML Layout', href: '/h26'},
];


export {
  htmlNavs
};
