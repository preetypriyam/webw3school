const topNav = [
  {text: 'HTML', href: '#'},
  {text: 'CSS', href: '#'},
  {text: 'JAVASCRIPT', href: '#'},
  {text: 'SQL', href: '#'},
  {text: 'PYTHON', href: '#'},
  {text: 'PHP', href: '#'},
  {text: 'BOOTSTRAP', href: '#'},
  {text: 'HOW TO', href: '#'},
  {text: 'W3.CSS', href: '#'},
  {text: 'JQUERY', href: '#'}
];

const topNavDropdown = [
  {text: 'MORE', href: '#'},
  {text: 'REFERENCE', href: '#'},
  {text: 'EXERCISE', href: '#'}
];

export {
  topNav,
  topNavDropdown
};
