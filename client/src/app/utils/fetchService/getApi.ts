import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

const BASE_URL = 'http://localhost:8899/';

@Injectable()
export class GetConstantsService {
  constructor(private http: HttpClient) {

  }

  getConstants(type: string): any {
    return this.http.get(`${BASE_URL}get_constants?type=${type}`);
  }
}
