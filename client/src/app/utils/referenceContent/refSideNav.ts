const refNavs = [
  {text: 'HTML by Alphabet', href: '#'},
  {text: 'HTML Category', href: '#'},
  {text: 'HTML Browser Support', href: '#'},
  {text: 'HTML Attributes', href: '#'},
  {text: 'HTML Global Attributes', href: '#'},
  {text: 'HTML Events', href: '#'},
  {text: 'HTML Colors', href: '#'},
  {text: 'HTML Canvas', href: '#'},
  {text: 'HTML Audio/Video', href: '#'},
  {text: 'HTML Character Sets', href: '#'},
  {text: 'HTML Doctypes', href: '#'},
  {text: 'HTML URL Encode', href: '#'},
  {text: 'HTML Language Codes', href: '#'},
  {text: 'HTML Country Codes', href: '#'},
  {text: 'HTML Messages', href: '#'},
  {text: 'HTML Methods', href: '#'},
  {text: 'PX to EM Converter', href: '#'},
  {text: 'Keyboard Shortcuts', href: '#'},
  {text: 'HTML by Alphabet', href: '#'},
  {text: 'HTML Category', href: '#'},
  {text: 'HTML Browser Support', href: '#'},
  {text: 'HTML Attributes', href: '#'},
  {text: 'HTML Global Attributes', href: '#'},
  {text: 'HTML Events', href: '#'},
  {text: 'HTML Colors', href: '#'},
  {text: 'HTML Canvas', href: '#'},
  {text: 'HTML Audio/Video', href: '#'},
  {text: 'HTML Character Sets', href: '#'},
  {text: 'HTML Doctypes', href: '#'},
  {text: 'HTML URL Encode', href: '#'},
  {text: 'HTML Language Codes', href: '#'},
  {text: 'HTML Country Codes', href: '#'},
  {text: 'HTML Messages', href: '#'},
  {text: 'HTML Methods', href: '#'},
  {text: 'PX to EM Converter', href: '#'},
  {text: 'Keyboard Shortcuts', href: '#'}
];

export {
  refNavs
};

