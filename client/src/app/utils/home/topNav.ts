import {IIconItem} from '../../types';

const topNavItem = [
  {text: 'TUTORIALS', href: '#'},
  {text: 'REFERENCES', href: '#'},
  {text: 'EXAMPLES', href: '#'},
  {text: 'EXERCISES', href: '#'},
];

const iconItems: IIconItem[] = [
  {aHref: '#', areaHidden: true, iconClass: 'fa fa-adjust'},
  {aHref: '#', areaHidden: true, iconClass: 'fa fa-globe'},
  {aHref: '#', areaHidden: false, iconClass: 'fa fa-fw fa-search'},
];


export {
  topNavItem,
  iconItems,
};
