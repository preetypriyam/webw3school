interface IReferenceItem {
  href: string;
  text: string;
}

export {
  IReferenceItem
};
