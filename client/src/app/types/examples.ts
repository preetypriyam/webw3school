interface IExampleItem {
  href: string;
  text: string;
}

export {
  IExampleItem
};
