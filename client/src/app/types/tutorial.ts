interface ITutorialItem {
  href: string;
  text: string;
}

export {
  ITutorialItem
};
