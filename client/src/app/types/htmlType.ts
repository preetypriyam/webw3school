interface TopNavItem {
  href: string;
  text: string;
}

interface IIconItem {
  aHref: string;
  iconClass: string;
  areaHidden: boolean;
}

interface INavItem {
  href: string;
  text: string;
}

export {
  INavItem,
  TopNavItem,
  IIconItem
};
