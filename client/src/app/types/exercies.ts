interface IExerciesItem {
  href: string;
  text: string;
}

export {
  IExerciesItem
};
