import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LearnHtmlComponent} from './pages/learn-html/learn-html.component';
import {HtmlReferenceComponent} from './pages/html-reference/html-reference.component';
import {RootContentComponent} from './pages/home/root-content.component';

const routes: Routes = [
  {path: '', component: RootContentComponent},
  {path: 'learn-html', component: LearnHtmlComponent},
  {path: 'html-reference', component: HtmlReferenceComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}


